import re


def change_str(txt):
    y = re.search('[qzwsxdcrfvtgbhnjmklp]$', txt)
    if y:
        # a = txt[len(txt) - 1]
        str1 = txt + ' ' + y.group() + 'ay'
    else:
        str1 = txt + ' way'
    return str1


# r - Read, a - Append, w - Write, x - Create
f1 = open('file1.txt', 'w')
f1.write('Thou blind fool, Love, what dost thou to mine eyes\nThat they behold, and see not what they see\nThey '
         'know what beauty is, see where it lies\nYet what the best is take the worst to be')
f1.close()

f2 = open('file1.txt', 'r')
f3 = open('file2.txt', 'w')
for x in f2:
    x = x.rstrip('\n')
    f3.write(change_str(x) + '\n')
f2.close()
f3.close()

name_file = input('Name of file: ')
try:
    f4 = open(name_file, 'r')
    print(f4.read().rstrip('\n'))
    f4.close()
except:
    print('Wrong name')













