Thou blind fool, Love, what dost thou to mine eyes
That they behold, and see not what they see
They know what beauty is, see where it lies
Yet what the best is take the worst to be