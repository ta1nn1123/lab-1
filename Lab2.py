import math


def circle_area(radius=45):
    area = math.pi * radius**2
    return area


def circle_circumference(radius):
    circumference = 2 * math.pi * radius
    return circumference


print(circle_circumference(57))
print(circle_circumference(radius=19))
print(circle_area(radius=49))
print(circle_area())
print(circle_area((lambda y, z: y / z)(8, 4)))
