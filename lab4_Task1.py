import math

radius = int(input("Введите радиус: "))
print("Радиус круга %d, площадь = %s - старый формат" % (radius, (radius*radius * math.pi)))
print('Площадь = {num} - новый формат '.format \
          (num= math.pi * radius * radius))